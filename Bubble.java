
public class Bubble {

    //сортировка пузырьком через рекурсию
    public static int[] sortRecursive(int[] nums,int length){
        if(length==0)
            return nums;

        nums=changeSiblingsToLastMax(nums,length);
        return  sortRecursive(nums, length - 1);//в итерации не учитывай всплывший
    }


    //стандартная сортировка пузырьком
    public static int[] sort(int[] nums){
        for (int i=nums.length;i>0;i--){
            for (int j=0;j<i;j++){
                nums=changeSiblingsToLastMax(nums,j);
            }
        }
        return nums;
    }

    //элемент с максимальным значением в массиве-всплыви
    private static int[] changeSiblingsToLastMax(int[] nums,int length){
        for(int i=0;i<length;i++){
            int tmp=nums[i+1];
            if(tmp<nums[i]){
                nums[i+1]=nums[i];
                nums[i]=tmp;
            }
        }
        return nums;
    }
}
