import java.util.Arrays;

public class Starter {
    public static void main(String[] args) {
        int nums[]={12,23,0,54,4,20,89};
        //пузырек рекурсивный
        System.out.println(Arrays.toString(Bubble.sortRecursive(nums, nums.length - 1)));
        //пузырек стандарт
        System.out.println(Arrays.toString(Bubble.sort(nums)));
        //сортировка выборкой
        System.out.println(Arrays.toString(Selection.sort(nums)));
    }
}
