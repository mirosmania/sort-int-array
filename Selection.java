//сортировка выборкой
public class Selection {
    public static int[] sort(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int current = nums[i];
            int pointer = i;
            for (int j = i + 1; j < nums.length; j++) {
                if (current > nums[j]) {
                    current = nums[j];
                    pointer = j;
                }
            }
            int tmp = nums[i];
            nums[i] = nums[pointer];
            nums[pointer] = tmp;
        }
        return nums;
    }
}
